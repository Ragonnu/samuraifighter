﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpRyu : MonoBehaviour
{
    public GameObject ryu;
    void Start()
    {
        ryu = GameObject.Find("Ryu");
        ryu.GetComponent<Ryu>().ryuHPEvent += UpdateHP;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void UpdateHP(float hp)
    {
        this.GetComponent<Image>().fillAmount = hp / 100;
    }
}
