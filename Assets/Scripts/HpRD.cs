﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HpRD : MonoBehaviour
{
    public GameObject ryuDevil;
    void Start()
    {
        ryuDevil = GameObject.Find("RyuDevil");
        ryuDevil.GetComponent<RyuDevil>().rdHPEvent += UpdateHP;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void UpdateHP(float hp)
    {
        this.GetComponent<Image>().fillAmount = hp / 100;
    }
}
