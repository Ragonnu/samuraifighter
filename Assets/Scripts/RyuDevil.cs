﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RyuDevil : MonoBehaviour
{
    public delegate void rdHP(float hp);
    public event rdHP rdHPEvent;
    [SerializeField]
    private float hp = 200;
    //public Image hpBar;
    public GameObject kickHurt;
    public GameObject punchHurt;
    [SerializeField]
    private int comboStatus = 0;
    private int force = 450;
    [SerializeField]
    private bool canJump = false;
    private int speed = 3;
    private Animator anim;
    public AudioClip clip;
    public GameObject adukenRD;
    [SerializeField]
    private bool guard = false;

    void Start()
    {
        //Con Mathf.Clamp asigno los valores maximos y minimos de una variable, en este caso hp.
        hp = Mathf.Clamp(hp, 0, 100);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        HpStatus();
        //Limits();
    }
    private void HpStatus()
    {

            if (hp <= 0)
            {
                if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
                {
                    SceneManager.LoadScene("Scene2");
                }
                else
                {
                    SceneManager.LoadScene("EndScene");
                }
            }
    }

    private void Move()
    {
        if (Input.GetKeyDown(KeyCode.O))
        {
            guard = true;
        }
        if (Input.GetKeyUp(KeyCode.O))
        {
            guard = false;
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            StartCoroutine(Combo("j"));
        }
        if (Input.GetKeyDown(KeyCode.U) && canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force));
            anim.SetBool("jump", true);
            canJump = false;
            StartCoroutine(Combo("u"));
        }
        if (Input.GetKeyDown(KeyCode.L))
        {
            if (!canJump)
            {
                print("entro en if");
                StartCoroutine(Combo("l"));
            }
            else
            {
                StartCoroutine(Kick());
            }
        }
        else if (Input.GetKeyDown(KeyCode.P))
        {
            if (comboStatus == 0)
            {
                StartCoroutine(Punch());
            }
            else
            {
                StartCoroutine(Combo("p"));
            }
        }
        if (Input.GetKey(KeyCode.K))
        {
            if (comboStatus == 0)
            {
                if (this.transform.position.x > 7.2f)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
                    anim.SetBool("back", true);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.H))
        {
            StartCoroutine(Combo("h"));
        }
        else if (Input.GetKey(KeyCode.H))
        {
            if (this.transform.position.x < -7.2f)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, GetComponent<Rigidbody2D>().velocity.y);
                anim.SetBool("walk", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.H))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            anim.SetBool("walk", false);
        }
        if (Input.GetKeyUp(KeyCode.K))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            anim.SetBool("back", false);
        }
    }
    IEnumerator Punch()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        punchHurt.SetActive(true);
        anim.SetBool("punch", true);
        yield return new WaitForSeconds(0.3f);
        punchHurt.SetActive(false);
        anim.SetBool("punch", false);
    }
    IEnumerator Kick()
    {
        print("entro en Kick");
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        kickHurt.SetActive(true);
        anim.SetBool("kick", true);
        yield return new WaitForSeconds(0.5f);
        kickHurt.SetActive(false);
        anim.SetBool("kick", false);
    }
    IEnumerator Combo(String s)
    {
        if (s == "u")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(1f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("Pulsaste tarde");
            }
        }
        if (s == "l")
        {
            if (comboStatus == 1)
            {
                kickHurt.SetActive(true);
                anim.SetBool("airAtack1", true);
                comboStatus = 0;
                yield return new WaitForSeconds(1f);
                kickHurt.SetActive(false);
                anim.SetBool("airAtack1", false);
            }
        }
        if (s == "j")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(0.2f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
            }
        }
        if (s == "h")
        {
            if (comboStatus == 1)
            {
                print("entro");
                comboStatus = 2;
                yield return new WaitForSeconds(0.2f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
            }
        }
        if (s == "p")
        {
            if (comboStatus == 2)
            {
                comboStatus = 0;
                AudioSource.PlayClipAtPoint(clip, this.transform.position);
                anim.SetBool("aduken", true);
                yield return new WaitForSeconds(0.5f);
                GameObject aduken = Instantiate(adukenRD);
                aduken.transform.position = new Vector2(transform.position.x - 3f, -1.5f);
                aduken.transform.localScale = new Vector2(aduken.transform.localScale.x, -aduken.transform.localScale.y);
                   // coso.transform.localscale = new Vector2(-coso.transform.localscale.x, coso.transform.localscale.y);
                anim.SetBool("aduken", false);
            }
            else
            {
                comboStatus = 0;
                print("Pulsaste tarde" + comboStatus);
            }
        }

    }
    private void Limits()
    {
        if (this.transform.position.x < -8.2f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (this.transform.position.x > 8.2f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //print("entro en colision");
        if (collision.gameObject.tag == "Floor")
        {
            //print("entro en if colision");
            anim.SetBool("jump", false);
            //anim.SetBool("airAtack1", false);
            canJump = true;
        }
        if (collision.gameObject.tag == "aduken" && guard == false)
        {
            print("quito vida");
            hp -= 30;
            if (rdHPEvent == null)
            {
                rdHPEvent.Invoke(hp);
            }
            rdHPEvent(hp);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "khryu" && guard == false)
        {
            print("quito vida patada");
            hp -= 10;
            if (rdHPEvent == null)
            {
                rdHPEvent.Invoke(hp);
            }
            rdHPEvent(hp);
        }
        if (collision.gameObject.tag == "phryu" && guard == false)
        {
            print("quito vida puño");
            hp -= 5;
            if (rdHPEvent == null)
            {
                rdHPEvent.Invoke(hp);
            }
            rdHPEvent(hp);
        }
    }
}
