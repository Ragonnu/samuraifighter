﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Aduken : MonoBehaviour
{
    [SerializeField]
    private int velocity = 5;
    public RyuDevil rd;

    // Start is called before the first frame update
    void Start()
    {
        rd = FindObjectOfType<RyuDevil>();
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        Delete();
        Speed();
    }

    private void Speed()
    {
        velocity++;
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(velocity, 0f);
    }

    private void Delete()
    {
        if(this.transform.position.x > 8.5f)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "RyuDevil")
        {
            Destroy(this.gameObject);
        }
    }
}
