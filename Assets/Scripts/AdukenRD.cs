﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdukenRD : MonoBehaviour
{
    private int velocity = 2;
    //public RyuDevil rd;

    // Start is called before the first frame update
    void Start()
    {
        //rd = FindObjectOfType<RyuDevil>();
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-velocity, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        Delete();
        Warp();
    }

    private void Warp()
    {
        this.transform.localScale = new Vector2(transform.localScale.x + 0.1f, transform.localScale.y + 0.1f);
    }

    private void Delete()
    {
        if (this.transform.position.x < -8.5f)
        {
            Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ryu")
        {
            Destroy(this.gameObject);
        }
    }
}
