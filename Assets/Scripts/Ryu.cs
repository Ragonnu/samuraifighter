﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ryu : MonoBehaviour
{
    [SerializeField]
    private float hp = 100f;
    public Image hpBar;
    public GameObject kickHurt;
    public GameObject punchHurt;
    [SerializeField]
    private int comboStatus = 0;
    private int force = 500;
    [SerializeField]
    private bool canJump = false;
    private int speed = 6;
    private Animator anim;
    public AudioClip clip;
    public GameObject aduken;
    public delegate void ryuHP(float hp);
    public event ryuHP ryuHPEvent;
    [SerializeField]
    private bool guard = false;
    void Start()
    {
        //Con Mathf.Clamp asigno los valores maximos y minimos de una variable, en este caso hp.
        hp = Mathf.Clamp(hp, 0, 100);
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        HpStatus();
        //Limits();
    }
    
    private void HpStatus()
    {
        if (hp <= 0)
        {
            if (SceneManager.GetActiveScene().name.Equals("SampleScene"))
            {
                SceneManager.LoadScene("Scene2");
            }
            else
            {
                SceneManager.LoadScene("EndScene");
            }
        }

    }

    private void Limits()
    {
        if (this.transform.position.x < -8.2f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }
        if (this.transform.position.x > 8.2f)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
        }
    }

    private void Move()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            guard = true;
        }
        if (Input.GetKeyUp(KeyCode.C))
        {
            guard = false;
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            StartCoroutine(Combo("s"));
        }
        if(Input.GetKeyDown(KeyCode.W) && canJump)
        {
            this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, force));
            anim.SetBool("jump", true);
            canJump = false;
            StartCoroutine(Combo("w"));
        }
        if (Input.GetKeyDown(KeyCode.V))
        {
            if (!canJump)
            {
                print("entro en if");
                StartCoroutine(Combo("v"));
            }
            else
            {
                StartCoroutine(Kick());
            }
        }
        else if (Input.GetKeyDown(KeyCode.B))
        {
            if(comboStatus == 0)
            {
                StartCoroutine(Punch());
            }
            else
            {
                StartCoroutine(Combo("b"));
            }
        }
        if (Input.GetKey(KeyCode.D))
        {
            if (comboStatus == 0)
            {
                if (this.transform.position.x > 7.2f)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
                }
                else
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, GetComponent<Rigidbody2D>().velocity.y);
                    anim.SetBool("walk", true);
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            StartCoroutine(Combo("d"));
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (this.transform.position.x < -7.2f)
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, GetComponent<Rigidbody2D>().velocity.y);
            }
            else
            {
                this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, GetComponent<Rigidbody2D>().velocity.y);
                anim.SetBool("back", true);
            }
        }

        if (Input.GetKeyUp(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            anim.SetBool("walk", false);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            anim.SetBool("back", false);
        }
    }
    IEnumerator Punch()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        punchHurt.SetActive(true);
        anim.SetBool("punch", true);
        yield return new WaitForSeconds(0.3f);
        punchHurt.SetActive(false);
        anim.SetBool("punch", false);
    }
    IEnumerator Kick()
    {
        print("entro en Kick");
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
        kickHurt.SetActive(true);
        anim.SetBool("kick", true);
        yield return new WaitForSeconds(0.5f);
        kickHurt.SetActive(false);
        anim.SetBool("kick", false);
    }
    IEnumerator Combo(String s)
    {
        if (s == "w")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(1f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("Pulsaste tarde");
            }
        }
        if (s == "v")
        {
            if (comboStatus == 1)
            {
                kickHurt.SetActive(true);
                anim.SetBool("airAtack1", true);
                comboStatus = 0;
                yield return new WaitForSeconds(1f);
                kickHurt.SetActive(false);
                anim.SetBool("airAtack1", false);
            }
        }
        if (s == "s")
        {
            if (comboStatus == 0)
            {
                comboStatus = 1;
                yield return new WaitForSeconds(0.2f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                //print("Pulsaste tarde la D");
            }
        }
        if (s == "d")
        {
            print("pulso la D");
            if (comboStatus == 1)
            {
                print("entro");
                comboStatus = 2;
                yield return new WaitForSeconds(0.2f);

                if (comboStatus == 1)
                {
                    comboStatus = 0;
                }
            }
            else
            {
                comboStatus = 0;
                print("Pulsaste tard la B");
            }
        }
        if (s == "b")
        {
            if (comboStatus == 2)
            {
                comboStatus = 0;
                AudioSource.PlayClipAtPoint(clip, this.transform.position);
                anim.SetBool("aduken", true);
                yield return new WaitForSeconds(0.5f);
                GameObject adukenInst = Instantiate(aduken, new Vector2(transform.position.x + 2f, -1.96f), Quaternion.identity);
                anim.SetBool("aduken", false);
            }
            else
            {
                comboStatus = 0;
                print("Pulsaste tarde" + comboStatus);
            }
        }

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //print("entro en colision");
        if(collision.gameObject.tag == "Floor" )
        {
            //print("entro en if colision");
            anim.SetBool("jump", false);
            //anim.SetBool("airAtack1", false);
            canJump = true;
        }
        if (collision.gameObject.tag == "adukenRD" && guard == false)
        {
            //print("quito vida");
            hp -= 10;
            if (ryuHPEvent == null)
            {
                ryuHPEvent.Invoke(hp);
            }
            ryuHPEvent(hp);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "khRD" && guard == false)
        {
            print("quito vida patada");
            hp -= 10;
            if (ryuHPEvent == null)
            {
                ryuHPEvent.Invoke(hp);
            }
            ryuHPEvent(hp);
        }
        if (collision.gameObject.tag == "phRD")
        {
            if (guard == false)
            {
                print("quito vida puño");
                hp -= 5;
                if (ryuHPEvent == null)
                {
                    ryuHPEvent.Invoke(hp);
                }
                ryuHPEvent(hp);
            }
        }
    }
}
